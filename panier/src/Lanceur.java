import java.util.ArrayList;

import collectionmetier.Collectionnable;
import factories.DAOFactory;
import factories.FactoryFactory;
import models.ConnexionBDD;
import models.Produit;

public class Lanceur {

	public static void main(String[] args) {
		
		
		
		ConnexionBDD.initBDD("panier", "panier");
		ConnexionBDD.getCon();
		ArrayList<Collectionnable> maListe = FactoryFactory.getFactory(FactoryFactory.DAOFACTORY).getDAO(DAOFactory.DAOPRODUIT).findAll().toArrayList();
		for (Collectionnable objenCours : maListe) {
			System.out.println(((Produit)objenCours).getDesigProduit());
		}
	}
}
