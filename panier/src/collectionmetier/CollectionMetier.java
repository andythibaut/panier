/**
 * 
 */
package collectionmetier;

import java.util.ArrayList;
import java.util.HashMap;

import exceptions.UOException;
import models.Couleur;
import models.Famille;
import models.Produit;
import models.SousFamille;


/**
 * @author andy
 *Classe collection métier, permet de récupérer des collections d'objets sour forme d'ArrayList
 */
public class CollectionMetier {

	private HashMap<String, Collectionnable> maListe = new HashMap<String, Collectionnable>();

	//ajouter à la collection un objet passé en paramètre 
	public void add(Collectionnable element) throws UOException {

		switch (element.getClass().getSimpleName()) {

		case "Produit": 
			this.maListe.put(((Produit) element).getRefProd(), element);
			break;
		case "Couleur":	
			this.maListe.put(((Couleur) element).getNomcouleur(), element);
			break;
		case "Famille": 	
			this.maListe.put(((Famille) element).getIntFam(), element);
			break;
		case "SousFamille":	
			this.maListe.put(((SousFamille) element).getIntSousFam(), element);
			break;
		default :
			throw new UOException("Objet non identifi�, appelez Mulder et Scully");
		}
	}

	//ajouter à la collection une autre passée en paramètre
	public void addAll(CollectionMetier c) {

		this.maListe.putAll(c.maListe);
	}

	public ArrayList<Collectionnable> toArrayList() {

		ArrayList<Collectionnable> maListe = new ArrayList<Collectionnable>();

		maListe.addAll(this.maListe.values());

		return maListe;
	}
}
