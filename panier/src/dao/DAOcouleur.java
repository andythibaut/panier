/**
 * 
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;

import collectionmetier.CollectionMetier;
import models.ConnexionBDD;
import models.Couleur;
import models.Produit;

/**
 * @author andy
 * DAOcouleur, implemente iDAO
 */
public class DAOcouleur implements IDAO<Couleur>{

	@Override
	public Couleur findByString(String uneRef) {
		// TODO Auto-generated method stub
		Couleur maCouleur = new Couleur();
		
		String req = "SELECT * FROM COULEUR WHERE CODEHEXACOUL = '" + uneRef + "'";
				
		try {
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery();
			monRS.next();
		
		//recupere les infos des colonnes de mon reusultset pour les set sur mon objet
			maCouleur.setNomcouleur(monRS.getString(1));
			maCouleur.setCodeHexaCoul(monRS.getString(2));
			maCouleur.setCodeR(monRS.getInt(3));
			maCouleur.setCodeV(monRS.getInt(4));
			maCouleur.setCodeB(monRS.getInt(5));
		 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return maCouleur;
	}


	@Override
	public Couleur create(Couleur pObjetACreer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Couleur update(Couleur pObjetAModifier) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Couleur pObjetASupprimer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Couleur findById(int pIdObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public CollectionMetier findAll() {
		CollectionMetier maListe = new CollectionMetier();
		// ecriture de la requete
		String req = "SELECT * FROM COULEUR";
			
		try {
			//connexion au server de la bdd
			Statement monPS = ConnexionBDD.getCon().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			//Ecriture des données dans la collectionMetier
			while (monRS.next()){
				maListe.add(new Produit (monRS.getString(1), monRS.getString(2), monRS.getString(3), monRS.getString(4)));
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return maListe;
	}


	@Override
	public CollectionMetier findByCriteria(Couleur pObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}

}
