/**
 * 
 */
package dao;

import java.sql.ResultSet;
import java.sql.Statement;

import collectionmetier.CollectionMetier;
import models.ConnexionBDD;
import models.Famille;
import models.Produit;
import models.Table;

/**
 * @author andy
 *
 */
public class DAOfamille implements IDAO<Famille>, Table{

	@Override
	public Famille create(Famille pObjetACreer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Famille update(Famille pObjetAModifier) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Famille pObjetASupprimer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Famille findById(int pIdObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectionMetier findByCriteria(Famille pIdObjetRecherche) {
		// TODO Auto-generated method stub
		
		return null;
	}


	@Override
	public CollectionMetier findAll() {
		CollectionMetier maListe = new CollectionMetier();
		// ecriture de la requete
		String req = "SELECT * FROM FAMILLE";
		
		
		try {
			//connexion au server de la bdd
			Statement monPS = ConnexionBDD.getCon().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			//Ecriture des données dans la collectionMetier
			while (monRS.next()){
				maListe.add(new Famille (monRS.getString(1), monRS.getString(2)));
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return maListe;
	}

	@Override
	public Famille findByString(String pIdObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
