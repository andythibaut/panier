/**
 * 
 */
package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import collectionmetier.CollectionMetier;
import models.ConnexionBDD;
import models.Produit;


/**
 * @author andy
 * DAO produit : permet l'ajout, la modification et l'accès aux données de produits
 * 
 */
public class DAOproduit implements IDAO<Produit>{

	@Override
	public Produit create(Produit objACreer) {

		String req = "INSERT INTO PRODUIT (REFPROD, DESIGPROD, INTFAM_FAM, INTSOUSFAM_SSFAM) VALUES (?, ?, ?, ?)";

		try {
					//connexion au server de la bdd
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);

					//recupere les valeurs de notre objets pour les metres dans notre requete
			monPS.setString	(1,  objACreer.getRefProd());
			monPS.setString	(2,  objACreer.getDesigProduit());
			monPS.setString	(3,  objACreer.getIntFam_Fam());
			monPS.setString	(4,  objACreer.getIntSousFam_SsFam());


			monPS.executeUpdate();

		} catch (SQLException e) {
			
			e.printStackTrace();
		}

		return objACreer; 
	}


	public Produit findByString(String uneRef) {
				
		Produit monProduit = new Produit();
		
		String req = "SELECT * FROM PRODUIT WHERE REFPROD = uneRef?";
				
		try {
			PreparedStatement monPS = ConnexionBDD.getCon().prepareStatement(req);
		
			ResultSet monRS = monPS.executeQuery();
		
			monRS.next();
		
		//recupere les infos des colonnes de mon reusultset pour les set sur mon objet
		
			monProduit.setRefProd(monRS.getString(1));   
			monProduit.setDesigProduit(monRS.getString(2));
			monProduit.setIntFam_Fam(monRS.getString(3));
			monProduit.setIntSousFam_SsFam(monRS.getString(4));

		 
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return monProduit;
	}


	@Override
	public Produit update(Produit pObjetAModifier) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public void delete(Produit pObjetASupprimer) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public Produit findById(int pIdObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public CollectionMetier findAll() {
		
		CollectionMetier maListe = new CollectionMetier();
		// ecriture de la requete
		String req = "SELECT * FROM PRODUIT";
		
		
		try {
			//connexion au server de la bdd
			Statement monPS = ConnexionBDD.getCon().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			//Ecriture des données dans la collectionMetier
			while (monRS.next()){
				maListe.add(new Produit (monRS.getString(1), monRS.getString(2), monRS.getString(3), monRS.getString(4)));
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return maListe;
		
	}



	@Override
	public CollectionMetier findByCriteria(Produit pObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
