/**
 * 
 */
package dao;

import java.sql.ResultSet;
import java.sql.Statement;

import collectionmetier.CollectionMetier;
import models.ConnexionBDD;
import models.Famille;
import models.SousFamille;

/**
 * @author andy
 *
 */
public class DAOsousFamille implements IDAO{

	@Override
	public Object create(Object pObjetACreer) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object update(Object pObjetAModifier) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(Object pObjetASupprimer) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Object findById(int pIdObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object findByString(String pIdObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CollectionMetier findAll() {
		CollectionMetier maListe = new CollectionMetier();
		// ecriture de la requete
		String req = "SELECT * FROM SOUS_FAMILLE";
		
		
		try {
			//connexion au server de la bdd
			Statement monPS = ConnexionBDD.getCon().prepareStatement(req);
			ResultSet monRS = monPS.executeQuery(req);
			//Ecriture des données dans la collectionMetier
			while (monRS.next()){
				maListe.add(new SousFamille (monRS.getString(1), monRS.getString(2)));
			}

		}catch (Exception e) {
			e.printStackTrace();
		}
		return maListe;
	}

	@Override
	public CollectionMetier findByCriteria(Object pObjetRecherche) {
		// TODO Auto-generated method stub
		return null;
	}

}
