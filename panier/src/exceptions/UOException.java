package exceptions;

public class UOException extends Exception {

	public UOException(String message) {
		super(message);
	}

	public UOException() {
		super();
	}
}
