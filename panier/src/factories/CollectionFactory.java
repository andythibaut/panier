package factories;

import collectionmetier.CollectionMetier;
import dao.IDAO;
import models.Table;

public class CollectionFactory extends ModeleFactory {

	@Override
	public IDAO getDAO(int pType) {
		return null;
	}

	@Override
	public Table getTable(int pType) {
		return null;
	}

	@Override
	public CollectionMetier getCollection() {
		return new CollectionMetier();
	}
}
