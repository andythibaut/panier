package factories;

import collectionmetier.CollectionMetier;
import dao.DAOproduit;
import dao.IDAO;
import models.Table;

public class DAOFactory extends ModeleFactory {

	//public static final int DAOPRODUIT		= 0;
	public static final int DAOPRODUIT		= 1;
	public static final int DAOHOTEL		= 2;
	public static final int DAOTYPEHOTEL	= 3;

	@Override
	public IDAO getDAO(int pType) {

		switch (pType) {

			case DAOPRODUIT :
				return new DAOproduit();
			/*case DAOCHAMBRE :
				return new DAOChambre();
			case DAOHOTEL :
				return new DAOHotel();
			case DAOTYPEHOTEL :
				return new DAOTypeHotel();*/
			default : 
				return null;
			}
	}

	@Override
	public Table getTable(int pType) {
		return null;
	}

	@Override
	public CollectionMetier getCollection() {
		return null;
	}
}
