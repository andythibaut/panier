package factories;

public class FactoryFactory {

	public static final int COLLECTIONFACTORY	= 0;
	public static final int DAOFACTORY			= 1;
	public static final int TABLEFACTORY		= 2;

	public static ModeleFactory getFactory(int pType) {

		switch(pType) {

			case COLLECTIONFACTORY :
				return new CollectionFactory();
			case DAOFACTORY :
				return new DAOFactory();
			case TABLEFACTORY :
				return new TableFactory();
			default :
				return null;
		}
	}
}
