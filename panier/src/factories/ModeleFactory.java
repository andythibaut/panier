package factories;

import collectionmetier.CollectionMetier;
import dao.IDAO;
import models.Table;

public abstract class ModeleFactory {

	public abstract IDAO getDAO(int pType);

	public abstract Table getTable(int pType);

	public abstract CollectionMetier getCollection();
}
