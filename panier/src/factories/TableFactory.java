package factories;

import collectionmetier.CollectionMetier;
import dao.IDAO;
import models.Couleur;
import models.Famille;
import models.Produit;
import models.SousFamille;
import models.Table;

public class TableFactory extends ModeleFactory {

	public static final int PRODUIT		= 0;
	public static final int COULEUR		= 1;
	public static final int FAMILLE		= 2;
	public static final int SSFAMILLE	= 3;

	@Override
	public IDAO getDAO(int pType) {
		return null;
	}

	@Override
	public Table getTable(int pType) {

		switch (pType) {

		case PRODUIT :
			return new Produit();
		case COULEUR :
			return new Couleur();
		case FAMILLE :
			return new Famille();
		case SSFAMILLE :
			return new SousFamille();
		default : 
			return null;
		}
	}

	@Override
	public CollectionMetier getCollection() {
		return null;
	}
}
