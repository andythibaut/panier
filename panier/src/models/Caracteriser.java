
/**
 *classe qui fait référence tous les prix des élastiques en fonction de leur 
 *model et leur couleur 
 */
package models;

/**
 * @author andy
 *
 */
public class Caracteriser {

	private int 	prix;
	private String 	refProd;
	private String 	nomCouleur_Car;
	
	public Caracteriser(int prix, String refProd, String nomCouleur_Car) {
		super();
		this.prix = prix;
		this.refProd = refProd;
		this.nomCouleur_Car = nomCouleur_Car;
	}
	
	public int getPrix() {
		return this.prix;
	}

	public void setPrix(int prix) {
		this.prix = prix;
	}

	public String getRefProd() {
		return this.refProd;
	}

	public void setRefProd(String refProd) {
		this.refProd = refProd;
	}

	public String getNomCouleur_Car() {
		return this.nomCouleur_Car;
	}

	public void setNomCouleur_Car(String nomCouleur_Car) {
		this.nomCouleur_Car = nomCouleur_Car;
	}
	
}
