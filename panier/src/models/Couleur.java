/**
 * 
 */
package models;

import collectionmetier.Collectionnable;

/**
 * @author andy
 * permet la construction de nouvelles couleurs dans la base
 */
public class Couleur implements Table, Collectionnable {

	private String 	nomcouleur;
	private String 	codeHexaCoul;
	private int 	codeR;
	private int		codeV;
	private int		codeB;
	
	
	//constructeurs
	public Couleur() {}
	
	public Couleur(String nomcouleur, String codeHexaCoul, int codeR, int codeV, int codeB) {
		super();
		this.nomcouleur = nomcouleur;
		this.codeHexaCoul = codeHexaCoul;
		this.codeR = codeR;
		this.codeV = codeV;
		this.codeB = codeB;
		
		
	//getters & setters
	}
	public String getNomcouleur() {
		return nomcouleur;
	}
	public void setNomcouleur(String nomcouleur) {
		this.nomcouleur = nomcouleur;
	}
	public String getCodeHexaCoul() {
		return codeHexaCoul;
	}
	public void setCodeHexaCoul(String codeHexaCoul) {
		this.codeHexaCoul = codeHexaCoul;
	}
	public int getCodeR() {
		return codeR;
	}
	public void setCodeR(int codeR) {
		this.codeR = codeR;
	}
	public int getCodeV() {
		return codeV;
	}
	public void setCodeV(int codeV) {
		this.codeV = codeV;
	}
	public int getCodeB() {
		return codeB;
	}
	public void setCodeB(int codeB) {
		this.codeB = codeB;
	}

	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	}

	
	
}
