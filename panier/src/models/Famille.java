/**
 * 
 */
package models;

import collectionmetier.Collectionnable;

/**
 * @author andy
 *
 */
public class Famille implements Collectionnable, Table{

	private String	intFam;
	private String	desgFam;
	
	public Famille() {
		super();
	}
		
	public Famille(String intFam, String desgFam) {
		super();
		this.intFam = intFam;
		this.desgFam = desgFam;
		
	}
	public String getIntFam() {
		return intFam;
	}
	public void setIntFam(String intFam) {
		this.intFam = intFam;
	}
	public String getDesgFam() {
		return desgFam;
	}
	public void setDesgFam(String desgFam) {
		this.desgFam = desgFam;
	}

	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
	
}
