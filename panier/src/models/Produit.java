/**
 * 
 */
package models;

import collectionmetier.Collectionnable;

/**
 * @author andy
 *
 */
public class Produit implements Table, Collectionnable{

	private String 	refProd;
	private String 	desigProduit;
	private String	intFam_Fam;
	private String intSousFam_SsFam;
	
	
	public Produit(String refProd, String desigProduit, String intFam_Fam, String intSousFam_SsFam) {
		super();
		this.refProd = refProd;
		this.desigProduit = desigProduit;
		this.intFam_Fam = intFam_Fam;
		this.intSousFam_SsFam = intSousFam_SsFam;
		
		
	}


	public Produit() {
		// TODO Auto-generated constructor stub
	}


	public String getRefProd() {
		return refProd;
	}


	public void setRefProd(String refProd) {
		this.refProd = refProd;
	}


	public String getDesigProduit() {
		return desigProduit;
	}


	public void setDesigProduit(String desigProduit) {
		this.desigProduit = desigProduit;
	}


	public String getIntFam_Fam() {
		return intFam_Fam;
	}


	public void setIntFam_Fam(String intFam_Fam) {
		this.intFam_Fam = intFam_Fam;
	}


	public String getIntSousFam_SsFam() {
		return intSousFam_SsFam;
	}


	public void setIntSousFam_SsFam(String intSousFam_SsFam) {
		this.intSousFam_SsFam = intSousFam_SsFam;
	}


	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	}
	
	
}
