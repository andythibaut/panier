/**
 * 
 */
package models;

import collectionmetier.Collectionnable;

/**
 * @author andy
 * classe des sous famille des produits
 */
public class SousFamille implements Table, Collectionnable{

	private String	intSousFam;
	private String	desgSousFam;
	
	//constructeur
	public SousFamille() {
		super();
	}
	
	public SousFamille(String intSousFam, String desgSousFam) {
		super();
		this.intSousFam = intSousFam;
		this.desgSousFam = desgSousFam;
	}

	//getters & setters
	public String getIntSousFam() {
		return intSousFam;
	}

	public void setIntSousFam(String intSousFam) {
		this.intSousFam = intSousFam;
	}

	public String getDesgSousFam() {
		return desgSousFam;
	}

	public void setDesgSousFam(String desgSousFam) {
		this.desgSousFam = desgSousFam;
	}

	@Override
	public String toTestString() {
		// TODO Auto-generated method stub
		return null;
	}

	
}
