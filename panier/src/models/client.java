/**classe client. fournis les informations relatives aux clients
 * 
 */
package models;

/**
 * @author andy
 *
 */
public class client {

	private String raisSocClient;
	private String addClient;
	private String cpClient;
	private String villeClient;
	
	public client(String raisSocClient, String addClient, String cpClient, String villeClient) {
		super();
		this.raisSocClient = raisSocClient;
		this.addClient = addClient;
		this.cpClient = cpClient;
		this.villeClient = villeClient;
		
	}
	public String getRaisSocClient() {
		return raisSocClient;
	}
	public void setRaisSocClient(String raisSocClient) {
		this.raisSocClient = raisSocClient;
	}
	public String getAddClient() {
		return addClient;
	}
	public void setAddClient(String addClient) {
		this.addClient = addClient;
	}
	public String getCpClient() {
		return cpClient;
	}
	public void setCpClient(String cpClient) {
		this.cpClient = cpClient;
	}
	public String getVilleClient() {
		return villeClient;
	}
	public void setVilleClient(String villeClient) {
		this.villeClient = villeClient;
	}
	
	
}
