/**
 * 
 */
package models;

/**
 * @author andy
 *
 */
public class fabriquant {

	private String 	raisSocFab;
	private String 	addFab;
	private String 	cpFab;
	private String 	vilFab;
	private int		delaisLivr;
	
	public fabriquant(String raisSocFab, String addFab, String cpFab, String vilFab, int delaisLivr) {
		super();
		this.raisSocFab = raisSocFab;
		this.addFab = addFab;
		this.cpFab = cpFab;
		this.vilFab = vilFab;
		this.delaisLivr = delaisLivr;
	}
	
	public String getRaisSocFab() {
		return raisSocFab;
	}
	public void setRaisSocFab(String raisSocFab) {
		this.raisSocFab = raisSocFab;
	}
	public String getAddFab() {
		return addFab;
	}
	public void setAddFab(String addFab) {
		this.addFab = addFab;
	}
	public String getCpFab() {
		return cpFab;
	}
	public void setCpFab(String cpFab) {
		this.cpFab = cpFab;
	}
	public String getVilFab() {
		return vilFab;
	}
	public void setVilFab(String vilFab) {
		this.vilFab = vilFab;
	}
	public int getDelaisLivr() {
		return delaisLivr;
	}
	public void setDelaisLivr(int delaisLivr) {
		this.delaisLivr = delaisLivr;
	}
	
}
