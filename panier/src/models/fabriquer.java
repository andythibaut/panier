/**
 * 
 */
package models;

/**
 * @author andy
 * Liste les références d'élastiques fabriqués par les fabricants
 */
public class fabriquer {

	private String	refProd;
	private String	raisSocFab;
	
	public fabriquer(String refProd, String raisSocFab) {
		super();
		this.refProd = refProd;
		this.raisSocFab = raisSocFab;
	}
	
	public String getRefProd() {
		return refProd;
	}
	public void setRefProd(String refProd) {
		this.refProd = refProd;
	}
	public String getRaisSocFab() {
		return raisSocFab;
	}
	public void setRaisSocFab(String raisSocFab) {
		this.raisSocFab = raisSocFab;
	}
	
}
