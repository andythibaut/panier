/**
 * 
 */
package models;

/**
 * @author andy
 *
 */
public class identifiants_Connexion {

	private String	id_User;
	private String	user_Password;
	private String	raisSocClient;
	
	public identifiants_Connexion(String id_User, String user_Password, String raisSocClient) {
		super();
		this.id_User = id_User;
		this.user_Password = user_Password;
		this.raisSocClient = raisSocClient;
	}

	public String getId_User() {
		return id_User;
	}

	public void setId_User(String id_User) {
		this.id_User = id_User;
	}

	public String getUser_Password() {
		return user_Password;
	}

	public void setUser_Password(String user_Password) {
		this.user_Password = user_Password;
	}

	public String getRaisSocClient() {
		return raisSocClient;
	}

	public void setRaisSocClient(String raisSocClient) {
		this.raisSocClient = raisSocClient;
	}
	
	
}
