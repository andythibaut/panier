/**
 * 
 */
package models;

/**
 * @author andy
 * classe permettant de générer un panier et y déposer des articles
 */
public class panier {

	private int		id_Panier;
	private boolean	etatPanier;
	private String	id_User;
	
	public panier(int id_Panier, boolean etatPanier, String id_User) {
		super();
		this.id_Panier = id_Panier;
		this.etatPanier = etatPanier;
		this.id_User = id_User;
	}

	public int getId_Panier() {
		return id_Panier;
	}

	public void setId_Panier(int id_Panier) {
		this.id_Panier = id_Panier;
	}

	public boolean isEtatPanier() {
		return etatPanier;
	}

	public void setEtatPanier(boolean etatPanier) {
		this.etatPanier = etatPanier;
	}

	public String getId_User() {
		return id_User;
	}

	public void setId_User(String id_User) {
		this.id_User = id_User;
	}
	
	
}
