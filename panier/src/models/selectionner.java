/**
 * 
 */
package models;

/**
 * @author andy
 *classe "selectionner" qui comprte la reference des produit, leur couleur ainsi que leur quantite misent dans un panier
 */
public class selectionner {

	private int 	quantiteProd;
	private int		id_Panier;
	private String	refProd;
	private String	nomCouleur_Coul;
	
	//constructeur
	public selectionner(int quantiteProd, int id_Panier, String refProd, String nomCouleur_Coul) {
		super();
		this.quantiteProd = quantiteProd;
		this.id_Panier = id_Panier;
		this.refProd = refProd;
		this.nomCouleur_Coul = nomCouleur_Coul;
	}

	public int getQuantiteProd() {
		return quantiteProd;
	}

	public void setQuantiteProd(int quantiteProd) {
		this.quantiteProd = quantiteProd;
	}

	public int getId_Panier() {
		return id_Panier;
	}

	public void setId_Panier(int id_Panier) {
		this.id_Panier = id_Panier;
	}

	public String getRefProd() {
		return refProd;
	}

	public void setRefProd(String refProd) {
		this.refProd = refProd;
	}

	public String getNomCouleur_Coul() {
		return nomCouleur_Coul;
	}

	public void setNomCouleur_Coul(String nomCouleur_Coul) {
		this.nomCouleur_Coul = nomCouleur_Coul;
	}
	
	//getters & setters
	
}
